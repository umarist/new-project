<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test(Request $request) {
        return response()->json([
            'success' => true,
            'data' => [
                'akjsdh0' => 'asdasd',
                'akjsdh1' => 'asdasd',
                'akjsdh2' => 'asdasd',
                'akjsdh3' => 'asdasd'
            ]
        ], 200);
    }
}
