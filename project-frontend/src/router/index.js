import Vue from 'vue'
import Router from 'vue-router'
import MainPage from "../components/MainPage";
import HelloWorld from "../components/HelloWorld";
import products from "./products";

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        { path: '/', name: 'MainPage', component: MainPage },
        { path: '/hello', name: 'HelloWorld', component: HelloWorld },
        ...products,
    ]
})
